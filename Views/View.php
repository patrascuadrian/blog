<?php

Class View {

	private $variables;

	public function assign($key, $value){
		$this->variables[$key] = $value;
	}

	public function render($method){
		echo "<!-- $method -->";
		$method = explode("::", $method);
		$controller = preg_replace('/Controller$/', '', $method[0]);
		$action = preg_replace('/Action$/', '', $method[1]);
		$variables = $this->variables;
		require "Views/$controller/$action.php";
	}
}