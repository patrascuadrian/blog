<?php

require  "Helpers/Autoload.php";

if (array_key_exists("C",$_GET)){
	$controller = $_GET["C"];
} else {
	$controller = "Default";
}

if (array_key_exists("A",$_GET)){
	$action = $_GET["A"];
} else {
	$action = "default";
} 

$controller = $controller."Controller";
$action = $action."Action";

$objectController = new $controller;
$objectController->$action();